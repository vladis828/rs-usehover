import { useRef, useState, useEffect } from 'react';

export function useHover() {
  const ref = useRef(null);
  const [hovered, setHovered] = useState(false);

  const onHoverHandler = () => {
    setHovered(true);
  };

  const onLeaveHandler = () => {
    setHovered(false);
  };

  useEffect(() => {
    const node = ref.current;

    if (node) {
      node.addEventListener('mouseover', onHoverHandler);
      node.addEventListener('mouseout', onLeaveHandler);

      return () => {
        node.removeEventListener('mouseover', onHoverHandler);
        node.removeEventListener('mouseout', onLeaveHandler);
      };
    }
  }, []);

  return { hovered, ref };
}
